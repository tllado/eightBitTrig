eightBitTrig is a very small, fast library that calculates basic trig functions on a scale from 1 to 255 at integer resolution. It is designed for use with microcontrollers or in other applications where floating point math is unavailable or undesirable.  

This includes:  
- *eightBitSine*: returns *127×sine(2πx/256)+128*  
- *eightBitCosine*: returns *127×cosine(2πx/256)+128*  
- *eightBitTangent*: returns *3×tangent(2πx/256)+128*  

This enables a value *a* to be scaled by *sine(x)* by typing  
`uint32_t b = a*eightBitSine(x)/255;`  

In other words, scaled as shown here:  

![alt text](https://raw.githubusercontent.com/tllado/eightBitTrig/master/funcs.jpg "eightBitTrig functions plotted")
